import { PeerServer } from 'peer';
import { readFileSync } from 'fs';

const app = PeerServer({
  port: +9000,
  allow_discovery: true,
  ssl: {
    key: readFileSync('/etc/letsencrypt/live/peer.aiacta.com/privkey.pem'),
    cert: readFileSync('/etc/letsencrypt/live/peer.aiacta.com/fullchain.pem'),
  },
});
console.log(`Server listening on port ${9000}`);
app._checkKey = function(
  key: string,
  ip: string,
  cb: (err: string | null) => void,
) {
  if (!this._clients[key]) {
    this._clients[key] = {};
  }
  if (!this._outstanding[key]) {
    this._outstanding[key] = {};
  }
  if (!this._ips[ip]) {
    this._ips[ip] = 0;
  }
  // Check concurrent limit
  if (
    Object.keys(this._clients[key]).length >= this._options.concurrent_limit
  ) {
    cb('Server has reached its concurrent user limit');
    return;
  }
  if (this._ips[ip] >= this._options.ip_limit) {
    cb(ip + ' has reached its concurrent user limit');
    return;
  }
  cb(null);
};
